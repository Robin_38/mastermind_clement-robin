#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tkinter as tk
import common
import codemaker1 as codemaker


class Box:
    def __init__(self, color, n, pos):
        # initialise les cases (elles sont grises au debut)
        self.color = color
        self.n = n    
        self.pos = pos
        self.x1 = pos*(diam+x_padding) + x_padding
        self.x2 = pos*(diam+x_padding) + x_padding + diam
        self.y1 = (Nmax-n)*(diam+y_padding)
        self.y2 = (Nmax-n)*(diam+y_padding)-diam
        self.circle = main_can.create_oval(self.x1, self.y1, self.x2, self.y2, fill='grey')
        
    def activeBtn(self):
        self.activateBtn = tk.Button(main_can, width=2, height=2, bg='white', command=self.displayColorChoice)
        self.activateBtn.place(x=self.x1, y=self.y1-diam)
        
    def displayColorChoice(self):
        # permet de choisir la couleur a affecter a la case
        self.activateBtn.config(text='X') # pour montrer que la case est selectionnee 
        self.cc_desc = tk.Label(right_can, text='Choisir une couleur')
        self.cc_desc.pack(side=tk.TOP)
        self.btncc_list=[]
        for color in COLORS:
            btn = tk.Button(right_can, text=color, command=lambda c=color : self.bindColor(c))
            self.btncc_list.append(btn)
            btn.pack()
    
    def bindColor(self, newcolor):
        # modifie la couleur d une case 
        global config
            
        self.activateBtn.destroy()
        self.cc_desc.destroy()
        for btncc in self.btncc_list:
            btncc.destroy()
            
        config[self.pos] = newcolor
        self.circle = main_can.create_oval(self.x1, self.y1, self.x2, self.y2, fill=newcolor)
        self.color = newcolor
        
    

class Eval:
    def __init__(self, n):
        # initialise toutes les zones d affichege des evaluations 
        self.n = n
        self.rx1 = (LENGTH+1)*(diam+x_padding) + x_padding
        self.rx2 = (LENGTH+1)*(diam+x_padding) + x_padding + diam
        self.ry1 = (Nmax-n)*(diam+y_padding)
        self.ry2 = (Nmax-n)*(diam+y_padding)-diam
        main_can.create_oval(self.rx1, self.ry1, self.rx2, self.ry2, fill='red', outline='black')
        self.wx1 = (LENGTH+2)*(diam+x_padding) + x_padding
        self.wx2 = (LENGTH+2)*(diam+x_padding) + x_padding + diam
        self.wy1 = (Nmax-n)*(diam+y_padding)
        self.wy2 = (Nmax-n)*(diam+y_padding)-diam
        main_can.create_oval(self.wx1, self.wy1, self.wx2, self.wy2, fill='white', outline='black')
    
    def setEval(self, config):
        # evalue config et affiche l eval 
        global n
        
        for i in range(len(config)):
            config[i] = bind_inv[config[i]]
            
        red, white = codemaker.codemaker(config)
        main_can.create_text(self.rx1+diam/2, self.ry1-diam/2, text=red)
        main_can.create_text(self.wx1+diam/2, self.wy1-diam/2, text=white, fill='black')
        if red == 4 :
            winAnim()
        n = n+1
        if n == Nmax:
            defeatAnim()
        play(n, config)
        
def winAnim():
    win_text = main_can.create_text(((diam+x_padding)*(LENGTH+3))/2, (Nmax*(diam+y_padding)+y_padding)/2, text='Gagné !', fill='black', font=('Helvetica','30','bold'))
        
def defeatAnim():
    defeat_text = main_can.create_text(((diam+x_padding)*(LENGTH+3))/2, (Nmax*(diam+y_padding)+y_padding)/2, text='Perdu !', fill='black', font=('Helvetica','30','bold'))
    
    
#%% Main

def play(n, config):
    # lance la tentative numero n
    global submit_btn
    submit_btn = tk.Button(bottom_can, text='SUBMIT', command=lambda:evals[n].setEval(config))
    submit_btn.place(x=(diam+x_padding)*(LENGTH+3)/2.2, y=50)
    for b in range(LENGTH):
        boxes[n][b].activeBtn()
    
def main():
    # lance une partie 
    global boxes, evals, config, solution, n
    
    main_can.place(x=0,y=0)
    bottom_can.place(x=0,y=Nmax*(diam+y_padding)+y_padding)
    right_can.place(x=(diam+x_padding)*(LENGTH+3),y=0)
    
    boxes = []
    evals = []
    codemaker.init() # choisi la solution initiale
    for n in range(Nmax):
        bline = []
        config = ['grey' for pos in range(LENGTH)] # on initialise toutes les configs
        for pos in range(LENGTH):
            b = Box('grey', n, pos) # et la grille
            bline.append(b)
            c = Eval(n)
        evals.append(c)
        boxes.append(bline)
    n = 0 # le numero de la tentative
    play(n, config)
    
def bind_colors(letters):
    full = []
    for letter in letters:
        full.append(bind[letter])
    return full
    
    
# Pour etre compatible avec la liste de couleurs de common.py
bind = {'B':'blue',
        'R':'red',
        'O':'orange',
        'M':'brown',
        'J':'yellow',
        'V':'green',
        'G':'grey',
        'N':'black'}

bind_inv = {v:k for k,v in bind.items()}

# parametres graphiques 
fen = tk.Tk()
diam = 40 # taille des cases
y_padding = 30 # espace entre les cases
x_padding = 30

# parametres de jeu
LENGTH = common.LENGTH # nb de couleurs dans une config
COLORS = bind_colors(common.COLORS) # nb de couleurs possibles
Nmax = 10 # nb de tentatives max 
        
# Organisation de la fenetre graphique 
main_can = tk.Canvas(fen, width=(diam+x_padding)*(LENGTH+3), height=Nmax*(diam+y_padding)+y_padding, bg='white')
bottom_can = tk.Canvas(fen, width=(diam+x_padding)*(LENGTH+3), height=100, bg='white')
right_can = tk.Canvas(fen, width=300, height=Nmax*(diam+y_padding)+y_padding, bg='white')

main()