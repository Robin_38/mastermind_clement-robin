#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import play
import codebreaker0
import codebreaker1
import codebreaker2
import codemaker1
import codemaker2

#%% Répartition du nombre d'essais avec codebreaker0 (complètement random)
# Question 3

fig, ax = plt.subplots()
N = 1000
esp0 = []
for rep in range(N):
    esp0.append(play.play(codemaker1, codebreaker0, quiet=True))
ax.hist(esp0, bins=100)
ax.set_ylabel('Nb essais')

#%% Comparaisons entre codebreakers et entre codemakers
    
def lissePlot(data, K):
    # fait une moyenne sur K points consécutifs pour avoir des courbes plus lisses
    N = len(data)
    lissed = []
    for i in range(N//K):
        lissed.append(sum(data[i*K:i*K+(K-1)])/K)
    return lissed
    
def compareBreakers(codemaker, codebreaker1, codebreaker2, N=100, K=5):
    # compare le nombre d'essais en faisant jouer les codebreakers1 et 2 contre le codemaker sur N parties. 
    fig, ax = plt.subplots()
    diff = np.zeros(N)
    for n in range(N):
        l1 = play.play(codemaker, codebreaker1, quiet=True)
        l2 = play.play(codemaker, codebreaker2, quiet=True)
        diff[n] = l1-l2
    
    diff = lissePlot(diff, K)
    
    ax.set_ylabel("Différence du nb d'essais (breaker1 - breaker2)")
    ax.set_xlabel('Nombre de tests (divisé par 5 pour lisser la courbe)')
    ax.hlines(xmin=0, xmax=N/K, y=0, color='black')
    ax.set_title('Comparaison des codebreakers avec codemaker1')
    ax.plot(diff)

def compareMakers(codebreaker, codemaker1, codemaker2, N=100, K=5):
    # compare le nombre d'essais en faisant jouer les codemaker1 et 2 contre le codebreaker sur N parties. 
    fig, ax = plt.subplots()
    diff = np.zeros(N)
    for n in range(N):
        l1 = play.play(codemaker1, codebreaker, quiet=True)
        l2 = play.play(codemaker2, codebreaker, quiet=True)
        diff[n] = l1-l2
    diff = lissePlot(diff, K)
    ax.set_ylabel("Différence du nb d'essais (maker1 - maker2)")
    ax.set_xlabel('Nombre de tests (divisé par 5 pour lisser la courbe)')
    ax.hlines(xmin=0, xmax=N/K, y=0, color='black')
    ax.set_title('Comparaison des codemakers avec codebreaker1')
    ax.plot(diff)

# Question 4
compareBreakers(codemaker1, codebreaker0, codebreaker1)

# Question 7
compareBreakers(codemaker1, codebreaker1, codebreaker2)

# Question 8
compareMakers(codebreaker1, codemaker1, codemaker2) # avec LENGTH=3 pour des temps raisonnables
