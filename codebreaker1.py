#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
import common
import itertools


def init():
    """
    Une fonction qui ne fait rien... pour cette version triviale.
    Pour vos codebreaker plus avancés, c'est ici que vous pouvez initialiser
    un certain nombre de variables à chaque début de partie.
    """
    global allcombs
    allcombs = []
    return

# Question 4
def codebreaker(evaluation_p):
    global allcombs
    while True:
        comb = random.choices(common.COLORS, k=common.LENGTH)
        if comb not in allcombs:
            allcombs.append(comb)
            return comb
    
