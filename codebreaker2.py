#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
import common

def init():
    global possibles
    test = ''.join(random.choices(common.COLORS, k=common.LENGTH)) # tente une combi completement aleatoire au debut
    return test 

def codebreaker(ev):
    """
    Parameters
    ----------
    ev : couple
        evalutation associée a la combi precedente
    """
    global possibles, prev, first_it 
    # possibles = ensembles des combis possibles avant cette iteration
    # prev = comb testee à l'iteration precedente
    # first_it = True si la premiere iteration a eu lieu
    if ev == None: # 1re iteration 
        prev = ''.join(random.choices(common.COLORS, k=common.LENGTH))
        first_it = True
        return prev
    
    if first_it: # 2eme iteration 
        possibles = common.donner_possibles(prev, ev)
        first_it = False
    
    else:
        possibles = common.maj_possibles(possibles, prev, ev)
        
    prev = random.choice(list(possibles))
    return prev
