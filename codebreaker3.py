#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import common
import itertools
import random

def init():
    global possibles, combi
    possibles = set(''.join(X) for X in itertools.product(common.COLORS, repeat=common.LENGTH))
    combi = random.choice(possibles) # 1ere tentative aleatoire
    return combi
    
def codebreaker(ev):
    # non fonctionnel (on aurait besoin d'avoir accès à la solution)
    global possibles, combi
    nmax = -1
    for p in possibles:
        cp = possibles.copy()
        ev_p = common.evaluation(combi, p)
        cp = common.maj_possibles(cp, combi, ev_p)
        n = len(cp)
        if n > nmax:
            nmax = n
            combi = p
            ev = ev_p
    possibles = common.maj_possibles(possibles, combi, ev)
    print(len(possibles))       
    return combi

print(init())
