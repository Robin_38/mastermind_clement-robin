#!/usr/bin/env python3
import itertools

COLORS = ['R', 'V', 'B', 'J', 'N', 'M', 'O', 'G']
LENGTH = 4

# Question 1
def evaluation(comb, ref):
    red, white = 0, 0
    seen_ref = [False for i in range(len(ref))]
    seen_comb = [False for i in range(len(ref))]
    
    for i, x in enumerate(comb): # red loop
        if ref[i] == x:
            red += 1
            seen_ref[i] = True
            seen_comb[i] = True

    for i, x in enumerate(comb): # white loop
        if x in ref:
            pos_in_ref = ref.index(x)
            if not seen_ref[pos_in_ref] and not seen_comb[i]:
                white += 1
                seen_ref[pos_in_ref] = True
                seen_comb[i] = True
                
    return red, white

# Question 5        
def donner_possibles(comb_testee, evalu):
    '''Prend en argument la combinaison testée et son évaluation associée
    Renvoie l'ensemble (set) des combinaisons possibles'''
    s = set()
    possibles = itertools.product(COLORS, repeat=LENGTH) # toutes les combinaisons (avec repetition, ordre important)
    for i in possibles:
        if evaluation(comb_testee, "".join(i)) == evalu:
            s.add("".join(i))
    return s
        
        
# Question 6
def maj_possibles(s, tested, evl):
    '''Prend en argument l'ensemble des possibles, la combinaison testée et son évaluation associée
    Renvoie l'ensemble (set) des combinaisons possibles'''
    invalids = []
    for comb in s:
        if evaluation(tested, comb) != evl:
            invalids.append(comb)
            
    for invalid_comb in invalids:
        s.discard(invalid_comb)
    return s 
    
#### TESTS ####
# s = donner_possibles('RRRV', (3, 0))
# print(len(s))
# print(s)

# maj_possibles('RRRB', (3, 0))

# print(len(s))
# print(s)


# ref = [0, 1, 2, 3]


# comb = [0, 1, 2, 3]
# print(evaluation(comb, ref))
# comb = [0, 1, 2, 4]
# print(evaluation(comb, ref))
# comb = [0, 1, 2, 0]
# print(evaluation(comb, ref))
# comb = [3, 2, 1, 0]
# print(evaluation(comb, ref))
# comb = [3, 3, 3, 0]
# print(evaluation(comb, ref))