#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import common

def check_codemaker(filename):
    """
    Parameters
    ----------
    filename : string
        log de la partie a checker.

    Returns
    -------
    bool
        True si pas de triche du codemaker visible. False si triche visible.
    """
    
    file = open(filename, 'r')
    game_log = file.read().splitlines()
    ref = game_log[-2]
    for essai in range(0, len(game_log), 2):
        comb = game_log[essai]
        evl = game_log[essai+1]
        evl = list(map(int, evl.split(','))) # pour transformer la list de str en list de int
        real_evl = common.evaluation(comb, ref)
        if real_evl[0] != evl[0] or real_evl[1] != evl[1]:
            return False
    return True

print(check_codemaker('log0.txt'))
