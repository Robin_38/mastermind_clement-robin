#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import common
import random
import itertools

def init():
    """
    Initialise l'ensemble des possibles et en choisi un aléatoirement
    """
    global possibles, ref
    ref = ''.join(random.choices(common.COLORS, k=common.LENGTH)) # reference initiale 
    possibles = set(''.join(X) for X in itertools.product(common.COLORS, repeat=common.LENGTH)) # toutes les combnaisons 
    ref = 'RNVB'
    print('Ref initiale : ', ref)
    return ref
    
def codemaker(combi):
    """
    Parameters
    ----------
    combi : string
        combinaison proposee par le codebreaker.

    Returns
    -------
    ref_evl : couple
        eval essociee a la meilleure ref calculee (celle qui laisse le plus de combinaisons valables).

    tres long mais optimal
    autre solution moins longue mais moins opti : faire comme dans codebreaker2 (un cas particulier pour la 1ere iteration qui 
    ne recalcule pas la ref opti). 
    """
    
    global possibles, ref
    nmax = -1
    for p in possibles:
        cp = possibles.copy()
        ev_p = common.evaluation(combi, p)
        cp = common.maj_possibles(cp, combi, ev_p)
        n = len(cp)
        if n > nmax:
            nmax = n
            ref = p
    ref_evl = common.evaluation(combi, ref)
    possibles = common.maj_possibles(possibles, combi, ref_evl) 
    return ref_evl


